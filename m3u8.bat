@ECHO OFF 
Title M3U8下载！
mode con cols=80 lines=30>nul 2>nul
color 9F
mode con cols=80 lines=30>nul 2>nul
@ECHO.
@rem 首先在mi3u8-list文件中填写m3u8地址，一行一个，然后运行此文件
@rem -L URI 设定下载M3U8地址，必须存在。可以xxx$http://xxx，或者http://xxx
@rem --wget_num=NUMBER 指定启动wget的数量，模拟多线程，默认1个。
@rem --tries=NUMBER 当文件数量下载不完全时指定重试次数，默认3次。
@rem --missing=NUMBER 当缺失文件少于多少时可以合并文件，默认5个。
@rem start 开启多个窗口，可以同时采集多个m3u8地址。（可自行修改）
for /f %%i in (m3u8-list.txt) do (
	IF %%i NEQ "" (
		php m3u8.php -L %%i --wget_num=99 --tries=10 --missing=5
		@rem START /min php m3u8.php -L %%i --wget_num=100 --tries=10 --missing=5
	)
)
