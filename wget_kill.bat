@ECHO OFF
TITLE 清理wget.exe进程
mode con cols=50 lines=5 >nul 2>nul
@rem 当下载出错，产生大量wget进程时使用
ECHO.
ECHO 正在清理wget.exe进程……
Process.exe -k wget.exe >NUL 2>NUL
EXIT 0
